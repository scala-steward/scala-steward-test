name := "scala-steward-test"

scalaVersion := "2.12.13"

libraryDependencies := List(
  "org.http4s" %% "http4s-core" % "0.21.21"
)